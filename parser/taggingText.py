#Open the grammar file and establish the vocabulary
import re
grammar = {}
grammar['word'] = {}
grammar['phrase'] = {}

with open("../grammar/grammar_alphaAAB.txt", "r") as f:
    for line in f:
        #Find the highest level rule
        if re.search('\#\?start:', line):
            line=line.split(sep=': ')
            grammar['start']=line[1].rstrip()
            continue
        #But skip any other commented lines
        elif line.startswith('#'):
            continue

        #Node labels contain : as definition
        elif re.search(':', line):
            line=line.split(sep=': ')
            #lowercase labels are phrase nodes;
            #uppercase labels are terminal nodes
            if re.search('[a-z]', line[0]):
                nodeType = 'phrase'
            else:
                nodeType = 'word'

            node = line[0]
            expansion = [line[1].rstrip()]
            grammar[nodeType][node]=expansion

        #Continuations of node labels start with |;
         #nodeType and node values hold over from however many lines
         #previous they were defined

        elif re.search('\|', line):
            line= line.split(sep='|')
            expansion.append(line[1].rstrip())
        else:
            continue
        grammar[nodeType][node] = expansion
f.close()


vocab = {}

for wordtype in grammar["word"].keys():
    vocab[wordtype] = []
    for word in grammar["word"][wordtype]:
        vocab[wordtype].append(word[1:-1])
        
#Open the file and read in the lines
lines = []

with open("../output/generated_text.txt", 'r') as f:
    for num, line in enumerate(f):
        lines.append(line.split(" "))
        
f.close()

for i in lines:
    i.pop(-2)

#Create tagging for the text using the vocabulary

tagged_lines = []

wordtypes = vocab.keys()

for line in lines:
    
    line_tagged = []
    
    for word in line:
        for wordtype in wordtypes:
            if word in vocab[wordtype]:
                line_tagged.append(wordtype)

    tagged_lines.append(line_tagged)

with open("../output/tagged_text.txt", 'w') as f:
    for line in tagged_lines:
        f.write(" ".join(line))
        f.write(" \n")
        
f.close()