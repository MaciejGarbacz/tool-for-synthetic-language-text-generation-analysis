# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 11:05:55 2019

@author: wnabi

Parser for testing grammar of a language

Documentation: https://lark-parser.readthedocs.io/en/latest/

See ./README.txt for more information

"""
import sys
from lark import Lark
import glob

grammars= ['grammar_alphaAAB.txt']

for gram in grammars:
    input_grammar_file = gram
    version = gram.split('_')[1].split('.')[0]

    path = '../output/tagged_text.txt'
    files= [path]
    print ("\nTesting for version :: *" + version + "*" + "\n")

    # read in the grammar, create parser
    grammar = ''
    with open(input_grammar_file, "r") as f:
        for line in f:
            if not line.startswith('#'):
                grammar = grammar + line

    pars = Lark(grammar, debug=True, start = 'sentence')

    for input_sentences_file in files:
    #read grammar, filter out comment lines (starting with #)d


        # process input file
        with open(input_sentences_file, "r") as f:

            counter_sentence=0
            counter_line=0
            sentences_lengths=[]
            successful_parsing_index = []


            for num, line in enumerate(f):

                counter_line += 1
                sentence = line.split(" ")
                sentences_lengths.append(len(line.split(" ")))

                if not line:
                    break

            #sentence
                try:
                    t = pars.parse(line)
                    successful_parsing_index.append(counter_line)
                    counter_sentence += 1
                #if not sentence, try other possibilities
                except:
                    continue

            print(input_sentences_file, '\n')
            print("Number of sentences parsed:", counter_sentence)
            print("Accuracy =", counter_sentence/(counter_line))

            unsuccessful_sentences_index = []
            unsuccessful_sentences_lengths = []
            successful_sentences_lengths = []

            print("Shortest successful sentence: ", min(successful_sentences_lengths))
            print("Shortest unsuccessful sentence: ", min(unsuccessful_sentences_lengths))

            print("Longest successful sentence: ", max(successful_sentences_lengths))
            print("Longest unsuccessful sentence: ", max(unsuccessful_sentences_lengths))


            print("Average unsuccessful sentence len: ", sum(unsuccessful_sentences_lengths)/len(unsuccessful_sentences_lengths))
            print("Average successful sentence len: ",sum(successful_sentences_lengths)/len(successful_sentences_lengths))

            print(unsuccessful_sentences_lengths)