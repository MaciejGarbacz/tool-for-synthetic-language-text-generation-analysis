# Read in the lines to be parsed
import copy

tagged_lines = []

with open("../output/tagged_text.txt", 'r') as f:
    for num, line in enumerate(f):
        tagged_lines.append(line)

f.close()

# Read in files with ss1 and ss2 structures

ss1_lines = []

with open("../grammar/structures/generated_ss1_alphaAAB.txt", 'r') as f:
    for num, line in enumerate(f):
        ss1_lines.append(line)

f.close()

ss2_lines = []

with open("../grammar/structures/generated_ss2_alphaAAB.txt", 'r') as f:
    for num, line in enumerate(f):
        ss2_lines.append(line)

f.close()

# Read in other files
substructures = {}

types = ["np", "vp", "pp"]

for t in types:
    substructures[t] = []
    with open("../grammar/structures/generated_" + t + "_alphaAAB.txt", 'r') as f:
        for num, line in enumerate(f):
            substructures[t].append(line[:-2])
    f.close()


def filter_for_unique(structure_indecies):
    unique_structures = []
    remaining = copy.deepcopy(structure_indecies)

    while len(remaining) != 0:
        longest_structure_len = 0
        longest_structure = (0, 0)
        for indecies in remaining:
            if (indecies[1] - indecies[0]) > longest_structure_len:
                longest_structure = indecies
                longest_structure_len = indecies[1] - indecies[0]

        remaining.remove(longest_structure)
        unique_structures.append(longest_structure)

        for i in range(len(structure_indecies)):
            current_tupule = structure_indecies[i]
            if current_tupule != longest_structure:
                if current_tupule[0] == longest_structure[0] or current_tupule[0] in range(longest_structure[0],
                                                                                           longest_structure[1]):
                    remaining.remove(current_tupule)

    return unique_structures


def partialParse(line):
    parsing_result = []
    for structure_type in substructures.keys():
        structure_indecies = []
        for structure in substructures[structure_type]:
            if structure in line:
                print(line, structure, structure_type)
                index = line.index(structure)
                structure_indecies.append((index, index + len(structure)))

        unique_structures = filter_for_unique(structure_indecies)
        for structure in unique_structures:
            parsing_result.append(structure_type)

    score = 0
    sentence_length = len(line.split(" "))
    for structure in parsing_result:
        if structure == 'pp':
            score += 1
        if structure == 'np':
            score += 2
        if structure == 'vp':
            score += 3
    score = score / sentence_length

    # print(score)
    return score


# Basic parsing - just ss1 and ss2

score = 0
for line in tagged_lines:
    parsed = False
    for ss1 in ss1_lines:
        if line == ss1:
            score += 1
            parsed = True
    for ss2 in ss2_lines:
        if line == ss2:
            score += 1
            parsed = True
    if not parsed:
        score += partialParse(line)

print("Model accuracy: " + str(score / len(tagged_lines)))
