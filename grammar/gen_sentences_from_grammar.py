# -*- coding: utf-8 -*-
"""
Created on Wed Aug 14 15:03:06 2019

@author: Clara

Modular grammar building, working from grammar_1AAA.text
"""

#Read in the grammar and store the phrase structure and morphological rules
import sys
import re
import random
from operator import itemgetter

#adjust version depending on grammar
version = "echoACB"

#form filenames depending on version
#both input files presumed to be available in the current folder
input_grammar_file = "grammar_" + version + ".txt"
output_sentence_file = "../training_data/generated_sentences_" + version + ".txt"
print ("\nBuilding sentences from grammar :: *" + version + "*" + "\n")

#read grammar, filter out comment lines (starting with #)d
grammar = {}
grammar['phrase']={}
grammar['word'] = {}


with open(input_grammar_file, "r") as f:
    for line in f:
        #Find the highest level rule
        if re.search('\#\?start:', line):
            line=line.split(sep=': ')
            grammar['start']=line[1].rstrip()
            continue
        #But skip any other commented lines

        elif line.startswith('#'):
            continue

        #Node labels contain : as definition
        elif re.search(':', line):
            line=line.split(sep=': ')
            #lowercase labels are phrase nodes;
            #uppercase labels are terminal nodes
            if re.search('[a-z]', line[0]):
                nodeType = 'phrase'
            else:
                nodeType = 'word'

            node = line[0]
            expansion = [line[1].rstrip()]
            grammar[nodeType][node]=expansion

        #Continuations of node labels start with |;
         #nodeType and node values hold over from however many lines
         #previous they were defined

        elif re.search('\|', line):
            line= line.split(sep='|')
            expansion.append(line[1].rstrip())
        else:
            continue
        grammar[nodeType][node] = expansion
f.close()

#Create the node-expansion function
def build_phrase(node):
    tree = {}
    nodes = random.choice(grammar['phrase'][node]).split(sep=' _W_ ')
    ix = 1
    for n in nodes:
        tagged_n = n+'.' +str(ix)
        tree[tagged_n]={}
        if re.search('^[a-z]', n):
            #strip out tag for looking up rule
            tree[tagged_n]['phrase']=build_phrase(n)
        else:
            tree[tagged_n]['word']=random.choice(grammar['word'][n])
            tree[tagged_n]['word']=re.sub('\"', '', tree[tagged_n]['word'])
        tree[tagged_n]['position'] = ix
        tree[tagged_n]['type'] = n
        ix += 1
    return(tree)

def linearize_phrase(tree):
    sentence = ''
    components = []
    for phrase in tree.keys():
        #print(phrase)
        node = [phrase, tree[phrase]['position']]
        #print(node)
        components.append(node)

    components.sort(key= itemgetter(1))
    #print(components)
    for node in components:
        #print(node)
        if re.search('[A-Z]', node[0]):
            #print(node, tree[node[0]]['word'])
            sentence = sentence + tree[node[0]]['word'] + ' '
        else:
            sentence = sentence + linearize_phrase(tree[node[0]]['phrase'])


    return(sentence)

#for i in range(10):
#    print(i, linearize_phrase(build_phrase('sentence')))

with open(output_sentence_file, 'w') as f:
    for i in range(20000):
        f.write(linearize_phrase(build_phrase('sentence')))
        f.write('\n')


f.close()

